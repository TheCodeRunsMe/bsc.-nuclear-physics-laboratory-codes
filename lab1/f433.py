"""
A script to analise Geiger Muller plateau.

Author: Özgür Özer
"""
import numpy as np
from sklearn.linear_model import ARDRegression
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
import pandas as pd
from os import chdir
import sys

print("coded with", sys.platform, "Python", sys.version)

count = np.array([224,241,218,241,227,214,236,256,235,256,251,215,244,309,258,
                 253,282,239,285,272,245,239,272,301,255,273,261,287,290,330,
                 299,400,428,611,676])

voltage = np.arange(35) * 25 + 350

ind = 31
print(f"Data is taken up to the index {ind}")

con = count[:ind]
vol = voltage[:ind, np.newaxis]

vol_train, vol_test, con_train, con_test = train_test_split(
    vol, con, test_size=.1, shuffle=True)

reg = ARDRegression(normalize=True)
reg.fit(vol_train, con_train)

print(f"Unbiased score is {reg.score(vol_test, con_test)}")
print("100 volt başına % eğim = ", 100 * reg.coef_[0] * 100 / count[0])
print("Çalışma voltajı: ", voltage[0], " - ", (voltage[ind] - voltage[0]) /
      3 + voltage[0])
plt.scatter(voltage, count, c="r")
plt.plot(vol, reg.predict(vol))
plt.xlabel("Voltage (V)")
plt.ylabel("Count")
chdir("/home/mergen/pg")
plt.savefig("plot.png", dpi=300, format="jpeg", pil_kwargs={'optimize': True})

Frame = pd.DataFrame(np.vstack((vol.squeeze(), con)).T,
                     columns=["Voltage", "Count"])
