"""Lab 4 script."""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import BayesianRidge as algorithm
import pandas as pd

data = np.array([14148, 7485, 4601, 2992, 2041, 1410, 1123, 805, 624, 510, 458,
                324, 300, 279, 227, 182, 173, 143, 159, 121, 94, 89, 97, 65,
                63, 61, 52, 50], dtype=np.float)

d = np.arange(3, 31, dtype=np.float)

# data is count / 10 s, here it is made count/s
nNoisy = data / 10
# The background noise 2.8 is eliminated
n = nNoisy - 2.8

error = np.sqrt(n)

# The radious of the error balls are errors of the points.
plt.scatter(d, n, c="xkcd:dark orange", s=error, label="Cesium Geiger count")
plt.xlabel("distance (cm)")
plt.ylabel("n ($s^{-1}$)")
plt.title("Cesium Geiger count vs distance with error(size of the point)")

# The change of variables to make inverse square law a linear func.
x = d**-2
# There is no intercept since in inverse square law, x^0 term is nonexistent
fitter = algorithm(fit_intercept=False)
fitter.fit(x.reshape(-1, 1), n, sample_weight=1/error)

pd.DataFrame(np.vstack((n,error)).T, index=d,
             columns=["Count", "error"]).to_excel("data.xlsx",
                                                  index_label="distance")

fit = fitter.predict(x.reshape(-1, 1))

print("The slope of the linear graph is: ", fitter.coef_[0])
# chi2 as specified in the class
chi2 = np.sum((fit - n) ** 2 / (n.size - 1))
print("chi squared is ", chi2)
plt.plot(d, fit, "--", label="Inverse-square fit")
plt.legend()
plt.savefig("lab4.png", dpi=300)
