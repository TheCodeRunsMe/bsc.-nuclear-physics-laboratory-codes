import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

data = np.array([150, 120, 145, 128, 137, 142, 130, 138, 122, 135, 142, 158,
                 138, 144, 144, 144, 136, 157, 137, 147, 162, 135, 157, 142,
                 147, 152, 151, 136, 142, 146, 141, 145, 140, 154, 150, 141,
                 137, 151, 122, 132, 136, 137, 127, 122, 146, 133, 142, 135,
                 150, 145, 135, 132, 148, 130, 152, 153, 141, 138, 154, 164])

mean = data.mean()
std = data.std()

print("The mean of the counts is: ", mean)
print("The standard deviation of the counts is: ", std)
print(f'The approximation of standard deviation is {np.sqrt(mean)}')

Frame = pd.DataFrame(data, index=range(1, 61))


def P(n):
    return 1 / (np.sqrt(2 * np.pi) * std) *\
        np.exp(- (n - mean) ** 2 / (2 * std ** 2))


fig, ax1 = plt.subplots()
n, bins, patches = ax1.hist(data)
ax1.set_xlabel("count")
ax1.set_ylabel("bins of counts")
ax2 = ax1.twinx()
ax2.set_ylabel("P(n)", color="tab:red")
x = np.linspace(data.min(), data.max(), 1000)
ax2.plot(x, P(x), color="tab:red")
ax2.tick_params(axis='y', labelcolor="tab:red")
fig.tight_layout()

plt.savefig("plot.png", format="png", dpi=300)

patches = np.array([np.sum(np.logical_and(mean - std < data,
                                          data < mean + std)),
                    np.sum(np.logical_and(mean - std * 2 < data,
                                          data < mean + std * 2)),
                    np.sum(np.logical_and(mean - std * 3 < data,
                                          data < mean + std * 3))]) / 60 * 100

print("std x (1,2,3) areas: by % inclusion", patches)

realData = data - 13.6
realmean = realData.mean()
realstd = realData.std()
print("The mean of the counts with background is: ", realmean)
print("The standard deviation of the counts with background is: ", realstd)
print("The error % considering background is: ",
      np.abs(mean - realmean) / realmean * 100)
