using XLSX, Statistics

data = XLSX.readxlsx("data.xlsx")
data = data["Sheet1"]
x = convert(Array{Float64}, data["A2:A12"])
data = convert(Array{Float64}, data["B2:D12"])
bg = [17, 17, 15]
bg = mean(bg)
data = mean(data, dims=2)
error = sqrt.(data)

