import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit

data = np.array([[850, 940, 896],
                 [566, 595, 606],
                 [507, 492, 515],
                 [471, 447, 465],
                 [376, 365, 356],
                 [333, 308, 330],
                 [242, 267, 249],
                 [213, 185, 190],
                 [151, 139, 140],
                 [93, 112, 97],
                 [70, 59, 90]])

x = np.array([0,
              1.74 / 13.6,
              3.4 / 13.6,
              (1.74 + 3.4) / 13.6,
              6.9 / 13.6,
              (1.74 + 6.9) / 13.6,
              (3.4+ 6.9) / 13.6,
              13.8 / 13.6,
              (3.4 + 13.8) / 13.6,
              (6.9 + 13.8) / 13.6,
              (13.8 + 13.8) / 13.6])

pd.DataFrame(data, index=x,
             columns=["Count 1", "Count 2", "Count 3"]).to_excel("data.xlsx",
                                                  index_label="thickness(cm)")
b = np.array([17, 17, 15])
bg = b.mean()
data = data - bg
data = data.mean(axis=1)
error = np.sqrt(data)

coefs, cov = curve_fit(lambda t,I,mu: I*np.exp(-mu * t),
                          x, data, sigma=error, absolute_sigma=True,
                          p0=[800,1])

print("Parameters P1 and P2 are :", coefs)
print("Half value thickness is: ", np.log(2)/coefs[1]+1, " cm")
plt.errorbar(x, data, error, fmt="none", capsize=3, label="counts")
plt.plot(x, coefs[0] * np.exp(-x * coefs[1]), c="g", label="fit")
plt.title("Gamma absorbtion")
plt.xlabel("thickness(cm)")
plt.ylabel("count")
plt.legend()
plt.savefig("lab6.png", dpi=300)
