using Plots
using DataFrames
using CSV

lld = collect(range(0.,4.9,step=.1))

int_count=[30813,30041, 26582, 26122, 25483, 24454, 23813, 23001, 22145, 21136, 20465,
      19370, 18329, 16698, 15475, 14559, 13930, 13062, 12371, 12029, 11335, 10855,
      10093, 9883, 9496, 9007, 8139, 7964, 7530, 7259, 7098, 7106, 7053, 6845, 6659,
      6820, 6625, 6116, 5411, 4591, 3193, 1830, 994, 939, 218, 138, 81, 58, 42, 38]

window_count=[9400, 9336, 9365, 8134, 8626, 8838, 8900, 9021, 9038, 8876, 8607, 8146, 7242,
         5839, 5441, 5026, 4811, 4781, 4403, 4077, 3752, 3368, 3046, 2601, 2234, 1710,
         1461, 1833, 2663, 3624, 5162, 6107, 6272, 6584, 6525, 6344, 5817, 4895, 3493,
         2071, 1197, 509, 215, 90, 30, 29, 28, 19, 5, 22]

p1 = plot(lld, int_count, xlabel="lld", ylabel="count", title="Integral Count")
p2 = plot(lld, window_count, xlabel="lld", ylabel="count", title="Differential Count")
savefig(plot(p1,p2, layout=(2,1), legend=false), "lab7.png")

CSV.write("lab7.csv",
    DataFrame(lld=lld, IntegralCount=int_count, DifferentialCount=window_count))



