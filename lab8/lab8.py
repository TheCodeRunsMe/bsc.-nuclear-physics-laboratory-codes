import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

bg = np.array([])

with open("al_background.txt", "r") as f:
    [f.readline() for _ in range(18)]
    for i in f:
        bg = np.append(bg, np.float(i))

data = np.array([])
with open("Al_notron.txt", "r") as fi:
    [fi.readline() for _ in range(18)]
    for i in fi:
        data = np.append(data, np.float(i))


def channel2energy(x):
    """Convert channel to energy in KeV."""
    return (1173 - 662) / (245 - 142) * (x - 142) + 662


def energy2channel(y):
    """Convert energy to channel"""
    return (y - 662) *(245 - 142) / (1173 - 662) + 142


channel = np.arange(1, 513)
pd.DataFrame(np.vstack((data, bg)).T, index=channel,
             columns=["data", "background"]).to_excel("lab8.xlsx")
energy = channel2energy(channel)
# counts without background
count = data - bg

count[count < 0] = 0
fig, ax = plt.subplots(constrained_layout=True, figsize=(16, 9))
ax.set_xticks(np.arange(1, 513, 18))
ax.bar(channel, count)
ax.set_xlabel("Channel")
ax.set_ylabel("Events")
secax = ax.secondary_xaxis('top', functions=(channel2energy, energy2channel))
secax.set_xlabel('energy (KeV)')
secax.set_xticks(np.arange(energy.min(), energy.max(), 100))
ax.set_title("Gamma Emission by Aluminum")
print("Energy corresponding to the channel 363 is ", channel2energy(363))
plt.grid()
plt.savefig("lab8.png", dpi=300)
